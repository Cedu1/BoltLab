$newHostname = "CLI01"
$currentHostname = hostname
Write-Output "Current hostname: $currentHostname"
Write-Output "Setting hostname to: $newHostname"

if(-not($currentHostname = $newHostname)) {
    Rename-Computer -NewName $newHostname -Restart 
}
else {
    Write-Output "Computer is already named $newHostname"
}
# Title: DHCP
# Comments: This script is used to set a DHCP Scope.
# Created by Bruce Reber
# Date: 12.02.2021
# Version: 0.1

#install dhcp
Install-WindowsFeature -Name DHCP -IncludeManagementTools

# Title: Disable UAC
# Comments: This script is used to disable the UAC.
# Created by Dorothea Vlad
# Date: 29.01.2021
# Version: 0.1

#Set UAC (User account control) to disabled. Computer reboot.
Set-ItemProperty -Path HKLM:Software\Microsoft\Windows\CurrentVersion\policies\system -Name EnableLUA -Value 0 -Force

# TEST RRA 12.04.2021
#New-ItemProperty -Path HKLM:Software\Microsoft\Windows\CurrentVersion\Policies\System -Name EnableLUA -PropertyType DWord -Value 0 -Force | Out-Null
#Write-Host "User Access Control (UAC) has been disabled." -ForegroundColor Green
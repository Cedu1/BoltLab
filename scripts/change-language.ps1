#Script to set the Windows settings
# Show LanguageList
$OldList = Get-WinUserLanguageList
# Add de-CH to LanguageList
$OldList.Add("de-CH")
Set-WinUserLanguageList -LanguageList $OldList -Force
Set-WinUserLanguageList -LanguageList de-CH -Force

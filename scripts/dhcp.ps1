# Title: DHCP
# Comments: This script is used to set a DHCP Scope.
# Created by Bruce Reber
# Date: 12.02.2021
# Version: 0.1


# dhcp scope variables
$scopename = "MyAutoScope"
$startrange = "192.168.56.40"
$endrange = "192.168.56.90"
$subnetmask = "255.255.255.0"
$scopeID = "192.168.56.0"
$regValue = "2"

# Creating scope
Add-DHCPServerv4Scope -EndRange $endrange -Name $scopename -StartRange $startrange -SubnetMask $subnetmask -State Active

# Set-DHCPServerv4OptionValue -ScopeId $scopeID
Set-DhcpServerv4OptionValue -ScopeId $scopeID -DnsServer 192.168.56.10
Set-ItemProperty -Path registry::HKEY_LOCAL_MACHINE\Software\Microsoft\ServerManager\Roles\12 -Name ConfigurationState -Value $regValue
